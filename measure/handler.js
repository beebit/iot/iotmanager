'use strict';

const NodeRSA = require('node-rsa');
const uuidv4 = require('uuid/v4');

let init = Date.now();
let text_key = "-----BEGIN RSA PRIVATE KEY-----\n" +
    "MIIEowIBAAKCAQEApT5VCpCfZ9pgV2Td5PGFnbYHCczYcY7ydOg9+8cs6VpD2x6F\n" +
    "pIZiM5wFZW52sVLRJztsECcziV8zhla+lUuxwCIbsWuOFoFLitmeWHBVeqz1KO3z\n" +
    "N6LkPSUD0mCJkYRrTKllkqNeacsTd9szzEWTgWFsEUdWcLZJnUsj2o8PGeK7VnhJ\n" +
    "/l3a2hvWtuZo5BcTcaeq44BXcAZzHjvlTFIDVHjJcbR5gqNsNxPPtIpxLVniuVBt\n" +
    "G2zukuk11vzwdW1WCmfs4gY/cP0MpcRUImkAnjYJAarh7TT5vngr14YfeFIv1Yv0\n" +
    "76QOPeVLOvnj6so+nlkor9cpfEwic6Vn2IHhSQIDAQABAoIBAG4Ge+9l7o3i7OUk\n" +
    "B09qtbIWQsd2uuOZ77yB6subij7yWF4tC4VJ6Vc92qX4EFXvs83wlfqHb5xXZGJd\n" +
    "ndagqQEqdNAEsn4vpxIT5ogye+gMA8016w0vi9NesxXAK7lVt3A7FqO13aaJjjPv\n" +
    "I6Uu6pFqQd+bZWX1KjzujDf5q29q4ypUE48YrRR7EEwC82tiIxPL1LI9ZL5SVXSF\n" +
    "8LZIygy8iEuRJzfanIVask2m3VWz/vQCbYvEds+ceiLpMdiXYAuHhRbLlBDf4+dw\n" +
    "48atMxkKZOgceMUCl7Qw6ovXeI+K+n6Zuu4DTrqpii5qFehExPKyUP6LuTQ7xbRz\n" +
    "MA4rYAECgYEA2xm4D1H3QvGcnRMfoNqsDWC/USsfr8ojIdmdtiTwzF47etEphkY8\n" +
    "OICE3ritAfHYicT+0ceauhUyfd9QEt2+94B4Qwi6Erp+aWZm3pe0+0vmRe/Gc4IS\n" +
    "DFGCLYCRNizmUNtilzLLFTDRragRWMB++TuDV9X/uXy1xl/k+abUfAECgYEAwRKg\n" +
    "nucot6/BKrV3+RvY+/PT2Ah9tk9tA2bY2sAAdIjQBjcfoM95NE/MTUrehpiAGBNo\n" +
    "z8qGhFK0QxLV6N0L2Xq9CYBnOGz/l6E3CaJsBASKkaQXVhrEnLJmZhRw+DOWkVjh\n" +
    "0HFNXIlnzshzet8jn5gAo0e36XXYlP5Q2dl+hUkCgYAdJ9n/Z+fgZ6hwmofd7ttw\n" +
    "WEx9L6Di0qJEMeIzx8ynVW7S1ByTeqsu1aNYw/h5xsYK50UFg2tKkeGLMHlGWW29\n" +
    "H8JhITspnNmPiwnrWwLBYgM9kT+2RTvDmeEZzv8D14u6fMcs2Zx+sGVml3PffJYo\n" +
    "8ktzF84Uz8ycnkwpEJM8AQKBgQCdSBto106oNJjbvfD6EeF1fm/48RHXzmQz/Thz\n" +
    "Gy5DwKAWMBmn2twV7Vu1K2STqO/QrR8ZBxcF/Im0ikeP52KbzAzrDJsJsGGWmFJJ\n" +
    "mvL7YuevNUl9b1jzfbFFPym4R+5EgW+TanlnBNh373uDgPt4K095tBbyLHo/Bd2/\n" +
    "WhpjYQKBgApkeCoIicJ3DxjX+B2E/gPWRHKHbX+Ph08Nq6S/tvPYmT6mM33sXAu2\n" +
    "DnCch6riGlX+JbXhQdnxiWBGF1nCvWg1MjvHmt0RAQrGF7Be3/uIMzDf/ye84Eog\n" +
    "Y7SETC6UaNGN5gRTeyYaKNe5wiLY+EFaBPgsbMq0dvXuxvsoste8\n" +
    "-----END RSA PRIVATE KEY-----";

let key = new NodeRSA();
key.importKey(text_key,'pkcs1');

console.log(key.exportKey('pkcs1'));
let pubkey = key.exportKey('pkcs1-public');

let header = JSON.stringify({
    "exp": Date.now() + (3600*1000*24*30), //Un mes
    "iss": "CE-79-C0-B3-07-59",
    "aud": "beemanager",
    "iat": Math.trunc(Date.now()/1000), //Date.now() devuelve ms
    "jti": "45745c60-7b1a-11e8-9c9c-2d42b21b1a3e", //Generas un uuid
});

let payload = JSON.stringify({
    "measure":24.5,
    "time": Date.now(),
    "battery": 66.5,
});

let token_without_signature = Buffer.from(header).toString('base64')+'.'+Buffer.from(payload).toString('base64');
let signature = key.sign(token_without_signature,'base64');
console.log(Buffer.from(header).toString('base64')+'.'+Buffer.from(payload).toString('base64')+'.'+signature);

key = new NodeRSA();
key.importKey(pubkey,'pkcs1-public');
console.log(pubkey);
console.log(key.verify(Buffer.from(token_without_signature).toString('base64'),signature,'base64','base64'));
console.log(Date.now() - init);
module.exports.measure = async (event, context) => {
    if (!event.hasOwnProperty('token') || !event.hasOwnProperty('timestamp')) {
        return {
            statusCode: 500,
            body: "Format error."
        }
    }

    let token = context.token.split('.');
    let header = JSON.parse(token[0]);
    let payload = JSON.parse(token[1]);
    let signature = JSON.parse(token[2]);
    let device_timestamp = context.timestamp;
    let timestamp = + Date.now();
    let pubkey = ''; // Requesting from database

    /*if(!key.verify(Buffer.from(token[0]+'.'+token[1]).toString('base64'),token[2],'base64','base64')){
        return {
            statusCode: 400,
            body: "Invalid token."
        }
    }*/

    const params = {
        TableName: process.env.DYNAMODB_TABLE,
        Measure: {
            id: uuidv4(),
            timestamp: timestamp,
            device_timestamp: device_timestamp,
            jti: header.jti,
            iss: header.iss,
            aud: header.aud,
            iat: header.iat,
            exp: header.expiration,
            payload: payload
        }
    };
    dynamoDb.put(params, (error) => {
        if (error) {
            console.error(error);
            callback(null, {
                statusCode: error.statusCode || 501,
                headers: { 'Content-Type': 'text/plain' },
                body: 'Couldn\'t create the item.'
            });
            return
        }
        const response = {
            statusCode: 200,
            body: JSON.stringify(params.Measure)
        };
        callback(null, response)
    });
};
